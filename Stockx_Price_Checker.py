import time
import re
import json
import requests
from bs4 import BeautifulSoup
import pandas as pd
import asyncio

print("Welcome to StockX Price Checker")
SKU = input("Input the SKU you want to search:\n")
print(F'Loading Data From SKU: {SKU}')
url = f"https://stockx.com/search/sneakers?s={SKU}"
api_url = "https://stockx.com/api/browse"

id_ = re.search(r"s=([a-zA-Z0-9\d-]+)", url).group(1)
params = {
    "": "",
    "currency": "EUR",
    "_search": id_,
    "dataType": "product",
}

headers = {
    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0",
    "Referer": url,
    'Accept-Language': 'en-US,en;q=0.5'
}

cookies = {
    'stockx_homepage': "sneakers",
    'stockx_selected_currency': "HKD",
    'stockx_market_country': 'HK',
    'stockx_selected_region': 'HK'
}

data = requests.get(api_url, params=params, headers=headers).json()

# uncomment this to print all data:
# print(json.dumps(data, indent=4))
list_url = []
list_bid_url = []
for product in data["Products"]:
    Final_URL = ("https://stockx.com/" + product["urlKey"])
    Bid_URL = ("https://stockx.com/zh-cn/sell/" + product["urlKey"])
    list_url.append(Final_URL)
    list_bid_url.append(Bid_URL)
    #print(Final_URL)
try:
    print(list_url[0],"\n")
except:
    print(f"Wrong SKU: {SKU}")

#print(list_bid_url[0])

time.sleep(1)

soup = BeautifulSoup(requests.get((list_url[0]), headers=headers, cookies=cookies).content,"html.parser")


print("Sleeping for 2 seconds...")
time.sleep(2)


try:
    for pic in soup.find(class_ = "product-content").find_all("img"):
        print("Loading Product Picture...")
        product_picture = (pic['src'])
        print(product_picture,"\n")
except:
        print("Fail to load Product Picture...")
        product_picture = "https://cdn.discordapp.com/attachments/781296985628999690/848060960899596318/5ba289e09f24ea6d65fc8a70_noimage.jpg"


list_info = []
try:
    print("Loading Product Details...")
    for info in soup.find(class_ = "product-details detail-column"):
        print(info.text)
        list_info.append(info.text)
#print(list_info)
except:
    print("Fail to load Product Details...")
    count = 0
    while count <=4 :
        list_info.append("Fail To Load Product Details ")
        count += 1

print(" ")

print("Loading Product Name...")
name = soup.find("h1", class_ = "name")
product_name = (name.text)
print(product_name)


list_size = []
sizes = soup.find("ul", class_ = "list-unstyled sneakers").find_all(class_ = 'title')
for size in sizes:
    list_size.append(size.text)
final_list_size = list_size[1:]
#print(final_list_size)

list_price = []
prices = soup.find("ul", class_ = "list-unstyled sneakers").find_all(class_ = 'subtitle')
for price in prices:
    list_price.append(price.text)
final_list_price = list_price[1:]
#print(final_list_price)

list_all = pd.DataFrame(
    {'Size': final_list_size,
    'Price': final_list_price,
    })

print(list_all)